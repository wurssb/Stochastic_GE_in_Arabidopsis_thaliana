#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <random>

using namespace std;
uniform_real_distribution<double> uniform(0.0,1.0); // U[0,1) distribution
random_device r_u;
seed_seq seed_u{r_u(), r_u(), r_u(), r_u(), r_u(), r_u(), r_u(), r_u()};
mt19937 gen_u(seed_u);
normal_distribution<double> gaussian(0.0,1.0);  // N(0,1) distribution
random_device r_n;
seed_seq seed_n{r_n(), r_n(), r_n(), r_n(), r_n(), r_n(), r_n(), r_n()};
mt19937 gen_n(seed_n);
random_device r_l;
seed_seq seed_l{r_l(), r_l(), r_l(), r_l(), r_l(), r_l(), r_l(), r_l()};
mt19937 gen_l(seed_l);
poisson_distribution<int> poisson_mRNA(2); // Poisson(v0/d0) distribution for initial mRNA abundance
random_device r_p;
seed_seq seed_p{r_p(), r_p(), r_p(), r_p(), r_p(), r_p(), r_p(), r_p()};
mt19937 gen_p(seed_p);


void update_propensities(double a[], double f[], int x[]){
    a[0]=f[0];
    a[1]=f[1]*x[0];
    a[2]=f[2]*x[0];
    a[3]=f[3]*x[1];
}

double propensity_sum(double a[], int n){
    int i;
    double s=0.0;
    for(i=0; i<n; i++){
        s += a[i];}
    return(s);
}

/************************************************************
 *                                                          *
 *                     Variables                            *
 *                                                          *
 ************************************************************/

//model parameters
int numReactions, numSpecies;
double f[4], a[4], a0, s, v1_m;
int M0, N0;

//simulation parameters
int numSigma, numN, numTraj;
double dt, dt2, tFinal, L;

/************************************************************
 *                                                          *
 *                   Main function                          *
 *                                                          *
 ************************************************************/

int main()
{
    //output files:
    ofstream posOut1("GFP_t1.txt");
    ofstream posOut2("GFP_t1_t1.txt");
    ofstream posOut3("GFP_t1_t2.txt");
    ofstream posOut4("Simulation_settings.txt");
    
    //reaction network:
    int numReactions=4;
    int S[4][2]={ // Stoichiometric matrix of the two-stage gene expression model
        { 1, 0},
        { 0, 1},
        {-1, 0},
        { 0,-1}};
    double v1=45; // average translation rate of the mother cells
    s=sqrt(100)/double(v1); //st.dev. of v1
    dt=L=0.1; // Extrande time-step and lookahead horizon
    dt2=0.1; // time steps at which the autocorrelation is calculated
    tFinal=55; // simulation time
    normal_distribution<double> gaussian2(0.0,s); // N(0,s) distribution
    
    //simulation parameters:
    int numSigmaM=pow(10,5);
    int rows=tFinal/dt;
    int rows_t=tFinal/dt2;
    
    posOut4<<"Variance of v1 = "<<pow(s*v1,2)<<endl;
    posOut4<<"numSigmaM = "<<numSigmaM<<endl;
    posOut4<<"Simulation time = "<<"["<<0<<":"<<dt<<":"<<tFinal<<"]"<<endl;
    posOut4<<"Autocorrelation time points = "<<tFinal/dt2<<endl;

    //INTRINSIC NOISE
    // Mean of individual trajectories
    int * N = (int*)malloc(rows*sizeof(int));
    double * GFPt1 = (double*)malloc(rows_t*sizeof(double));
    double * GFPt1old = (double*)malloc(rows_t*sizeof(double));
    double * GFPt2 = (double*)malloc(rows_t*sizeof(double));
    double * GFPt2old = (double*)malloc(rows_t*sizeof(double));

    //Mean of covariance
    double * GFPt1GFPt2 = (double*)malloc(rows_t*rows_t*sizeof(double));
    double * GFPt1GFPt2old = (double*)malloc(rows_t*rows_t*sizeof(double));
    double * GFPt1GFPt1 = (double*)malloc(rows_t*sizeof(double));
    double * GFPt1GFPt1old = (double*)malloc(rows_t*sizeof(double));
    double * GFPt2GFPt2 = (double*)malloc(rows_t*sizeof(double));
    double * GFPt2GFPt2old = (double*)malloc(rows_t*sizeof(double));

    memset(GFPt1,0,rows_t*sizeof(double));
    memset(GFPt1old,0,rows_t*sizeof(double));
    memset(GFPt2,0,rows_t*sizeof(double));
    memset(GFPt2old,0,rows_t*sizeof(double));
    memset(GFPt1GFPt2,0,rows_t*sizeof(double));
    memset(GFPt1GFPt2old,0,rows_t*sizeof(double));
    memset(GFPt1GFPt1,0,rows_t*sizeof(double));
    memset(GFPt1GFPt1old,0,rows_t*sizeof(double));
    memset(GFPt2GFPt2,0,rows_t*sizeof(double));
    memset(GFPt2GFPt2old,0,rows_t*sizeof(double));

    for (int k=1; k<=numSigmaM; k++) {
        memset(N,0,rows*sizeof(int));
        double s_m=gaussian2(gen_l);
        v1_m=v1*exp(s_m-(pow(s,2)/double(2.0)));
        double f[4]={2.25, v1_m, 1.125, 0.09}; // reaction rates
        M0=poisson_mRNA(gen_p); // initial number of mRNA molecules
        N0=0; // initial number of protein molecules
        int x[2] = {M0, N0};
        update_propensities(a, f, x);
        for (int step=0; step<=rows; step++) {
            double t = step * dt;
            //Extrande loop:
            double s=t;
            double tOut=t+dt;
            while (true) {
                //1) Evaluate propensities
                a0=propensity_sum(a, numReactions);
                //2) Generate tau
                double urnd1=uniform(gen_u);
                double tau=log(1/urnd1)/a0;
                //3) Advance time
                if (s+tau>tOut) {
                    break;
                }
                if (tau>L) {
                    s+=L;
                    continue;
                }
                //4) Advance time
                s+=tau;
                //5) Choose reaction
                update_propensities(a, f, x);
                a0=propensity_sum(a, numReactions);
                
                double urnd2=uniform(gen_u);
                double r = a0*urnd2;
                
                if (a0>=r){
                    // Select reaction
                    double psum=a[0];
                    int reaction=0;
                    while (psum<r){
                        reaction+=1;
                        psum+=a[reaction];
                    }
                    // Update state
                    x[0]+=S[reaction][0]; //M
                    x[1]+=S[reaction][1]; //GFP1
                }
            }
            N[step]=x[1];
        }
        for (int r=0; r<rows_t; r++){
            int t1=r*dt2*(1/dt);
            GFPt1[r]=GFPt1old[r]+((N[t1]-GFPt1old[r])/double(k));
            GFPt1GFPt1[r]=GFPt1GFPt1old[r]+((N[t1]*N[t1]-GFPt1GFPt1old[r])/double(k));
            GFPt1old[r]=GFPt1[r];
            GFPt1GFPt1old[r]=GFPt1GFPt1[r];
            for (int q=0; q<rows_t; q++){
                int t2=q*dt2*(1/dt);
                GFPt1GFPt2[rows_t*r+q]=GFPt1GFPt2old[rows_t*r+q]+((N[t1]*N[t2]-GFPt1GFPt2old[rows_t*r+q])/double(k));
                GFPt1GFPt2old[rows_t*r+q]=GFPt1GFPt2[rows_t*r+q];
            }
        }
    }
    for (int r=0; r<rows_t; r++) { //Print mean of numSigma parameters
        posOut1<<GFPt1[r]<<endl;
        posOut2<<GFPt1GFPt1[r]<<endl;
        for (int q=0; q<rows_t; q++) {
            posOut3<<GFPt1GFPt2[rows_t*r+q]<<"\t";
        }
        posOut3<<endl;
    }
    delete GFPt1;
    delete GFPt1old;
    delete GFPt1GFPt1;
    delete GFPt1GFPt1old;
    delete GFPt1GFPt2;
    delete GFPt1GFPt2old;
    posOut1.close();
    posOut2.close();
    posOut3.close();
    posOut4.close();
    exit(0);
}





