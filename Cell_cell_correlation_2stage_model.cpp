#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <random>

using namespace std;
uniform_real_distribution<double> uniform(0.0,1.0); // U[0,1) distribution
normal_distribution<double> gaussian(0.0,1.0);  // N(0,1) distribution
random_device r_u;
seed_seq seed_u{r_u(), r_u(), r_u(), r_u(), r_u(), r_u(), r_u(), r_u()}; // seed
mt19937 gen_u(seed_u); // generator
random_device r_n;
seed_seq seed_n{r_n(), r_n(), r_n(), r_n(), r_n(), r_n(), r_n(), r_n()}; // seed
mt19937 gen_n(seed_n); // generator
random_device r_l;
seed_seq seed_l{r_l(), r_l(), r_l(), r_l(), r_l(), r_l(), r_l(), r_l()}; // seed
mt19937 gen_l(seed_l); // generator
poisson_distribution<int> poisson_mRNA(2); // Poisson(v0/d0) distribution for initial mRNA abundance
random_device r_p;
seed_seq seed_p{r_p(), r_p(), r_p(), r_p(), r_p(), r_p(), r_p(), r_p()}; // seed
mt19937 gen_p(seed_p); // generator

void update_cdf(double par[], double Pn2[], double Cn2[], double sums[], int nmax, double pi){ // CDF of steady-state protein distribution
    double a=par[0]/par[3]; // v0/d1
    double b=par[1]/par[2]; // v1/d0
    for (int k = 0; k < nmax-1; k++){
        if (k==0){
            sums[k]=log(a);
        }
        else {
            sums[k] = sums[k-1]+log(a + k);
        }
    }
    for (int n=1; n<nmax; n++){
        Pn2[n]=(1/sqrt(2*pi*n))*exp(-n*(log(n/b)-1)-(a+n)*log(1+b)+sums[n-1]);
        Cn2[n]=Cn2[n-1] + Pn2[n];
    }
}

void update_propensities(double a[], double f[], int x[]){
    // reporter pair 1: CFP1, YFP1
    a[0]=f[0];
    a[1]=f[1]*x[0];
    a[2]=f[2]*x[0];
    a[3]=f[3]*x[1];
    a[4]=f[0];
    a[5]=f[1]*x[2];
    a[6]=f[2]*x[2];
    a[7]=f[3]*x[3];
    
    // reporter pair 2: CFP2, YFP2
    a[8]=f[4];
    a[9]=f[5]*x[4];
    a[10]=f[6]*x[4];
    a[11]=f[7]*x[5];
    a[12]=f[4];
    a[13]=f[5]*x[6];
    a[14]=f[6]*x[6];
    a[15]=f[7]*x[7];
}

double propensity_sum(double a[], int n){
    int i;
    double s=0.0;
    for(i=0; i<n; i++){
        s += a[i];}
    return(s);
}

/************************************************************
 *                                                          *
 *                     Variables                            *
 *                                                          *
 ************************************************************/
//constants
const double pi = 3.1415926535897;

//model parameters
int numReactions, numSpecies;
double f[8], a[16], a0, s, v1_m, v1_d1, v1_d2;
int M1, M2, N1, N2;

//simulation parameters
int numSigma, numN, numTraj;
double dt, tFinal, L;
int nmax;
double Cm2[23]={0.00000000000000,0.13533528323661,0.40600584970984,0.67667641618306,0.85712346049855,0.94734698265629,0.98343639151939,0.99546619447375,0.99890328103214,0.99976255267174,0.99995350192498,0.99999169177563,0.99999863538484,0.99999979265304,0.99999997069430,0.99999999612877,0.99999999952003,0.99999999994394,0.99999999999381,0.99999999999935,0.99999999999994,0.99999999999999,1.00000000000000}; // CDF of the steady-state mRNA distribution


/************************************************************
 *                                                          *
 *                   Main function                          *
 *                                                          *
 ************************************************************/

int main()
{
    //output files:
    ofstream posOut1("EX.txt");
    ofstream posOut2("Simulation_settings.txt");
    
    //reaction network:
    int numReactions=16;
    int S[16][8]={
        { 1, 0, 0, 0, 0, 0, 0, 0},
        { 0, 1, 0, 0, 0, 0, 0, 0},
        {-1, 0, 0, 0, 0, 0, 0, 0},
        { 0,-1, 0, 0, 0, 0, 0, 0},
        { 0, 0, 1, 0, 0, 0, 0, 0},
        { 0, 0, 0, 1, 0, 0, 0, 0},
        { 0, 0,-1, 0, 0, 0, 0, 0},
        { 0, 0, 0,-1, 0, 0, 0, 0},
        { 0, 0, 0, 0, 1, 0, 0, 0},
        { 0, 0, 0, 0, 0, 1, 0, 0},
        { 0, 0, 0, 0,-1, 0, 0, 0},
        { 0, 0, 0, 0, 0,-1, 0, 0},
        { 0, 0, 0, 0, 0, 0, 1, 0},
        { 0, 0, 0, 0, 0, 0, 0, 1},
        { 0, 0, 0, 0, 0, 0,-1, 0},
        { 0, 0, 0, 0, 0, 0, 0,-1}};
    double v1=500; // average translation rate of the mother cells
    s=sqrt(1000)/double(v1); //st.dev. of v1
    dt=L=0.01;
    tFinal=500;
    normal_distribution<double> gaussian2(0.0,s); //N(0,0.1) distribution (mean, st.dev.)
    
    //simulation parameters:
    int numSigmaM=2;//pow(10,6);
    int numSigmaD=1;
    int numN=1;
    int numTraj=1;
    int rows=tFinal/dt;
    int nmax=2700;
    
    posOut2<<"Variance of sigma = "<<pow(s*v1,2)<<endl;
    posOut2<<"numSigmaM = "<<numSigmaM<<endl;
    posOut2<<"numSigmaD = "<<numSigmaD<<endl;
    posOut2<<"numN = "<<numN<<endl;
    posOut2<<"numTraj = "<<numTraj<<endl;

    double * Pn2 = (double*)malloc(nmax*sizeof(double));
    double * Cn2 = (double*)malloc(nmax*sizeof(double));
    double * sums = (double*)malloc((nmax-1)*sizeof(double));
    memset(Pn2,0,nmax*sizeof(double));
    memset(Cn2,0,nmax*sizeof(double));
    memset(sums,0,(nmax-1)*sizeof(double));
    
    //INTRINSIC NOISE
    // Mean of individual trajectories
    double * CFP1_NSS = (double*)malloc(rows*sizeof(double));    //x[0]
    double * CFP1_NSSold = (double*)malloc(rows*sizeof(double));
    double * YFP1_NSS = (double*)malloc(rows*sizeof(double));    //x[1]
    double * YFP1_NSSold = (double*)malloc(rows*sizeof(double));
    double * CFP2_NSS = (double*)malloc(rows*sizeof(double));    //x[2]
    double * CFP2_NSSold = (double*)malloc(rows*sizeof(double));
    double * YFP2_NSS = (double*)malloc(rows*sizeof(double));    //x[3]
    double * YFP2_NSSold = (double*)malloc(rows*sizeof(double));
    //Mean of extrinsic noise
    double * CFP1YFP1_NSS = (double*)malloc(rows*sizeof(double)); //x[0]*x[1]
    double * CFP1YFP1_NSSold = (double*)malloc(rows*sizeof(double));
    double * CFP2YFP2_NSS = (double*)malloc(rows*sizeof(double)); //x[2]*x[3]
    double * CFP2YFP2_NSSold = (double*)malloc(rows*sizeof(double));
    //Mean of covariance
    double * CFP1YFP2_NSS = (double*)malloc(rows*sizeof(double)); //x[0]*x[3]
    double * CFP1YFP2_NSSold = (double*)malloc(rows*sizeof(double));
    double * CFP2YFP1_NSS = (double*)malloc(rows*sizeof(double)); //x[1]*x[2]
    double * CFP2YFP1_NSSold = (double*)malloc(rows*sizeof(double));
    double * CFP1CFP2_NSS = (double*)malloc(rows*sizeof(double)); //x[0]*x[2]
    double * CFP1CFP2_NSSold = (double*)malloc(rows*sizeof(double));
    double * YFP1YFP2_NSS = (double*)malloc(rows*sizeof(double)); //x[1]*x[3]
    double * YFP1YFP2_NSSold = (double*)malloc(rows*sizeof(double));
    
    //INITIAL CONDITIONS
    // Mean of individual trajectories
    double * CFP1_SS = (double*)malloc(rows*sizeof(double));    //x[0]
    double * CFP1_SSold = (double*)malloc(rows*sizeof(double));
    double * YFP1_SS = (double*)malloc(rows*sizeof(double));    //x[1]
    double * YFP1_SSold = (double*)malloc(rows*sizeof(double));
    double * CFP2_SS = (double*)malloc(rows*sizeof(double));    //x[2]
    double * CFP2_SSold = (double*)malloc(rows*sizeof(double));
    double * YFP2_SS = (double*)malloc(rows*sizeof(double));    //x[3]
    double * YFP2_SSold = (double*)malloc(rows*sizeof(double));
    //Mean of extrinsic noise
    double * CFP1YFP1_SS = (double*)malloc(rows*sizeof(double)); //x[0]*x[1]
    double * CFP1YFP1_SSold = (double*)malloc(rows*sizeof(double));
    double * CFP2YFP2_SS = (double*)malloc(rows*sizeof(double)); //x[2]*x[3]
    double * CFP2YFP2_SSold = (double*)malloc(rows*sizeof(double));
    //Mean of covariance
    double * CFP1YFP2_SS = (double*)malloc(rows*sizeof(double)); //x[0]*x[3]
    double * CFP1YFP2_SSold = (double*)malloc(rows*sizeof(double));
    double * CFP2YFP1_SS = (double*)malloc(rows*sizeof(double)); //x[1]*x[2]
    double * CFP2YFP1_SSold = (double*)malloc(rows*sizeof(double));
    double * CFP1CFP2_SS = (double*)malloc(rows*sizeof(double)); //x[0]*x[2]
    double * CFP1CFP2_SSold = (double*)malloc(rows*sizeof(double));
    double * YFP1YFP2_SS = (double*)malloc(rows*sizeof(double)); //x[1]*x[3]
    double * YFP1YFP2_SSold = (double*)malloc(rows*sizeof(double));
    
    //SIGMAS (DAUGHTERS)
    // Mean of individual trajectories
    double * CFP1_S = (double*)malloc(rows*sizeof(double));    //x[0]
    double * CFP1_Sold = (double*)malloc(rows*sizeof(double));
    double * YFP1_S = (double*)malloc(rows*sizeof(double));    //x[1]
    double * YFP1_Sold = (double*)malloc(rows*sizeof(double));
    double * CFP2_S = (double*)malloc(rows*sizeof(double));    //x[2]
    double * CFP2_Sold = (double*)malloc(rows*sizeof(double));
    double * YFP2_S = (double*)malloc(rows*sizeof(double));    //x[3]
    double * YFP2_Sold = (double*)malloc(rows*sizeof(double));
    //Mean of extrinsic noise
    double * CFP1YFP1_S = (double*)malloc(rows*sizeof(double)); //x[0]*x[1]
    double * CFP1YFP1_Sold = (double*)malloc(rows*sizeof(double));
    double * CFP2YFP2_S = (double*)malloc(rows*sizeof(double)); //x[2]*x[3]
    double * CFP2YFP2_Sold = (double*)malloc(rows*sizeof(double));
    //Mean of covariance
    double * CFP1YFP2_S = (double*)malloc(rows*sizeof(double)); //x[0]*x[3]
    double * CFP1YFP2_Sold = (double*)malloc(rows*sizeof(double));
    double * CFP2YFP1_S = (double*)malloc(rows*sizeof(double)); //x[1]*x[2]
    double * CFP2YFP1_Sold = (double*)malloc(rows*sizeof(double));
    double * CFP1CFP2_S = (double*)malloc(rows*sizeof(double)); //x[0]*x[2]
    double * CFP1CFP2_Sold = (double*)malloc(rows*sizeof(double));
    double * YFP1YFP2_S = (double*)malloc(rows*sizeof(double)); //x[1]*x[3]
    double * YFP1YFP2_Sold = (double*)malloc(rows*sizeof(double));
    
    //SIGMAS (MOTHER)
    // Mean of individual trajectories
    double * CFP1 = (double*)malloc(rows*sizeof(double));    //x[0]
    double * CFP1old = (double*)malloc(rows*sizeof(double));
    double * YFP1 = (double*)malloc(rows*sizeof(double));    //x[1]
    double * YFP1old = (double*)malloc(rows*sizeof(double));
    double * CFP2 = (double*)malloc(rows*sizeof(double));    //x[2]
    double * CFP2old = (double*)malloc(rows*sizeof(double));
    double * YFP2 = (double*)malloc(rows*sizeof(double));    //x[3]
    double * YFP2old = (double*)malloc(rows*sizeof(double));
    //Mean of extrinsic noise
    double * CFP1YFP1 = (double*)malloc(rows*sizeof(double)); //x[0]*x[1]
    double * CFP1YFP1old = (double*)malloc(rows*sizeof(double));
    double * CFP2YFP2 = (double*)malloc(rows*sizeof(double)); //x[2]*x[3]
    double * CFP2YFP2old = (double*)malloc(rows*sizeof(double));
    //Mean of covariance
    double * CFP1YFP2 = (double*)malloc(rows*sizeof(double)); //x[0]*x[3]
    double * CFP1YFP2old = (double*)malloc(rows*sizeof(double));
    double * CFP2YFP1 = (double*)malloc(rows*sizeof(double)); //x[1]*x[2]
    double * CFP2YFP1old = (double*)malloc(rows*sizeof(double));
    double * CFP1CFP2 = (double*)malloc(rows*sizeof(double)); //x[0]*x[2]
    double * CFP1CFP2old = (double*)malloc(rows*sizeof(double));
    double * YFP1YFP2 = (double*)malloc(rows*sizeof(double)); //x[1]*x[3]
    double * YFP1YFP2old = (double*)malloc(rows*sizeof(double));

    memset(CFP1,0,rows*sizeof(double));
    memset(CFP1old,0,rows*sizeof(double));
    memset(YFP1,0,rows*sizeof(double));
    memset(YFP1old,0,rows*sizeof(double));
    memset(CFP2,0,rows*sizeof(double));
    memset(CFP2old,0,rows*sizeof(double));
    memset(YFP2,0,rows*sizeof(double));
    memset(YFP2old,0,rows*sizeof(double));
    memset(CFP1YFP1,0,rows*sizeof(double));
    memset(CFP1YFP1old,0,rows*sizeof(double));
    memset(CFP2YFP2,0,rows*sizeof(double));
    memset(CFP2YFP2old,0,rows*sizeof(double));
    memset(CFP1YFP2,0,rows*sizeof(double));
    memset(CFP1YFP2old,0,rows*sizeof(double));
    memset(CFP2YFP1,0,rows*sizeof(double));
    memset(CFP2YFP1old,0,rows*sizeof(double));
    memset(CFP1CFP2,0,rows*sizeof(double));
    memset(CFP1CFP2old,0,rows*sizeof(double));
    memset(YFP1YFP2,0,rows*sizeof(double));
    memset(YFP1YFP2old,0,rows*sizeof(double));
    

    
    for (int h=1; h<=numSigmaM; h++) { //LOOP OVER SIGMAS (MOTHER CELL)
        double s_m=gaussian2(gen_l);
        v1_m=v1*exp(s_m-(pow(s,2)/double(2.0)));
        memset(CFP1_S,0,rows*sizeof(double));
        memset(CFP1_Sold,0,rows*sizeof(double));
        memset(YFP1_S,0,rows*sizeof(double));
        memset(YFP1_Sold,0,rows*sizeof(double));
        memset(CFP2_S,0,rows*sizeof(double));
        memset(CFP2_Sold,0,rows*sizeof(double));
        memset(YFP2_S,0,rows*sizeof(double));
        memset(YFP2_Sold,0,rows*sizeof(double));
        memset(CFP1YFP1_S,0,rows*sizeof(double));
        memset(CFP1YFP1_Sold,0,rows*sizeof(double));
        memset(CFP2YFP2_S,0,rows*sizeof(double));
        memset(CFP2YFP2_Sold,0,rows*sizeof(double));
        memset(CFP1YFP2_S,0,rows*sizeof(double));
        memset(CFP1YFP2_Sold,0,rows*sizeof(double));
        memset(CFP2YFP1_S,0,rows*sizeof(double));
        memset(CFP2YFP1_Sold,0,rows*sizeof(double));
        memset(CFP1CFP2_S,0,rows*sizeof(double));
        memset(CFP1CFP2_Sold,0,rows*sizeof(double));
        memset(YFP1YFP2_S,0,rows*sizeof(double));
        memset(YFP1YFP2_Sold,0,rows*sizeof(double));
        for (int i=1; i<=numSigmaD; i++) { //LOOP OVER SIGMAS (DAUGHTER CELLS)
            double s1_i=gaussian2(gen_l);
            double s2_i=gaussian2(gen_l);
            v1_d1=v1*exp(s1_i-(pow(s,2)/double(2.0)));
            v1_d2=v1*exp(s2_i-(pow(s,2)/double(2.0)));
            double f[8]={25, v1_d1, 12.5, 1, 25, v1_d2, 12.5, 1}; // reaction rates
            memset(CFP1_SS,0,rows*sizeof(double));
            memset(CFP1_SSold,0,rows*sizeof(double));
            memset(YFP1_SS,0,rows*sizeof(double));
            memset(YFP1_SSold,0,rows*sizeof(double));
            memset(CFP2_SS,0,rows*sizeof(double));
            memset(CFP2_SSold,0,rows*sizeof(double));
            memset(YFP2_SS,0,rows*sizeof(double));
            memset(YFP2_SSold,0,rows*sizeof(double));
            memset(CFP1YFP1_SS,0,rows*sizeof(double));
            memset(CFP1YFP1_SSold,0,rows*sizeof(double));
            memset(CFP2YFP2_SS,0,rows*sizeof(double));
            memset(CFP2YFP2_SSold,0,rows*sizeof(double));
            memset(CFP1YFP2_SS,0,rows*sizeof(double));
            memset(CFP1YFP2_SSold,0,rows*sizeof(double));
            memset(CFP2YFP1_SS,0,rows*sizeof(double));
            memset(CFP2YFP1_SSold,0,rows*sizeof(double));
            memset(CFP1CFP2_SS,0,rows*sizeof(double));
            memset(CFP1CFP2_SSold,0,rows*sizeof(double));
            memset(YFP1YFP2_SS,0,rows*sizeof(double));
            memset(YFP1YFP2_SSold,0,rows*sizeof(double));
            for (int j=1; j<=numN; j++) { //LOOP OVER INTIAL CONDITIONS
                double u1=uniform(gen_u);
                double u2=uniform(gen_u);
                double u3=uniform(gen_u);
                double u4=uniform(gen_u);
                for (int i=0; i<=23; i++) {
                    if (Cm2[i]>u1) {
                        M1=i-1;
                        break;
                    } else {
                        continue;
                    }
                }
                for (int i=0; i<=23; i++) {
                    if (Cm2[i]>u2) {
                        M2=i-1;
                        break;
                    } else {
                        continue;
                    }
                }
                double par[4]={f[0],v1_m,f[2],f[3]};
                update_cdf(par, Pn2, Cn2, sums, nmax, pi);
                for (int i=0; i<=nmax; i++) {
                    if (Cn2[i]>u3) {
                        N1=i-1;
                        break;
                    } else {
                        continue;
                    }
                }
                for (int i=0; i<=nmax; i++) {
                    if (Cn2[i]>u4) {
                        N2=i-1;
                        break;
                    } else {
                        continue;
                    }
                }
                memset(CFP1_NSS,0,rows*sizeof(double));
                memset(CFP1_NSSold,0,rows*sizeof(double));
                memset(YFP1_NSS,0,rows*sizeof(double));
                memset(YFP1_NSSold,0,rows*sizeof(double));
                memset(CFP2_NSS,0,rows*sizeof(double));
                memset(CFP2_NSSold,0,rows*sizeof(double));
                memset(YFP2_NSS,0,rows*sizeof(double));
                memset(YFP2_NSSold,0,rows*sizeof(double));
                memset(CFP1YFP1_NSS,0,rows*sizeof(double));
                memset(CFP1YFP1_NSSold,0,rows*sizeof(double));
                memset(CFP2YFP2_NSS,0,rows*sizeof(double));
                memset(CFP2YFP2_NSSold,0,rows*sizeof(double));
                memset(CFP1YFP2_NSS,0,rows*sizeof(double));
                memset(CFP1YFP2_NSSold,0,rows*sizeof(double));
                memset(CFP2YFP1_NSS,0,rows*sizeof(double));
                memset(CFP2YFP1_NSSold,0,rows*sizeof(double));
                memset(CFP1CFP2_NSS,0,rows*sizeof(double));
                memset(CFP1CFP2_NSSold,0,rows*sizeof(double));
                memset(YFP1YFP2_NSS,0,rows*sizeof(double));
                memset(YFP1YFP2_NSSold,0,rows*sizeof(double));
                CFP1_NSS[0]=N1;
                CFP2_NSS[0]=N1;
                YFP1_NSS[0]=N2;
                YFP2_NSS[0]=N2;
                CFP1YFP1_NSS[0]=N1*N2;
                CFP2YFP2_NSS[0]=N1*N2;
                CFP1YFP2_NSS[0]=N1*N2;
                CFP2YFP1_NSS[0]=N1*N2;
                CFP1CFP2_NSS[0]=N1*N1;
                YFP1YFP2_NSS[0]=N2*N2;
                for (int k=1; k<=numTraj; k++) {// SIMULATE INDIVIDUAL TRAJECTORIES
                    int x[8] = {M1, N1, M2, N2, M1, N1, M2, N2};
                    update_propensities(a, f, x);
                    for (int step=1; step<=rows; step++) {
                        double t = step * dt;
                        //Extrande loop:
                        double s=t;
                        double tOut=t+dt;
                        while (true) {
                            //1) Evaluate propensities
                            a0=propensity_sum(a, numReactions);
                            //2) Generate tau
                            double urnd1=uniform(gen_u);
                            double tau=log(1/urnd1)/a0;
                            //3) Advance time
                            if (s+tau>tOut) {
                                break;
                            }
                            if (tau>L) {
                                s+=L;
                                continue;
                            }
                            //4) Advance time
                            s+=tau;
                            //5) Choose reaction
                            update_propensities(a, f, x);
                            a0=propensity_sum(a, numReactions);
                            
                            double urnd2=uniform(gen_u);
                            double r = a0*urnd2;
                            
                            if (a0>=r){
                                // Select reaction
                                double psum=a[0];
                                int reaction=0;
                                while (psum<r){
                                    reaction+=1;
                                    psum+=a[reaction];
                                }
                                // Update state
                                x[0]+=S[reaction][0]; //mRNA CFP1
                                x[1]+=S[reaction][1]; //CFP1
                                x[2]+=S[reaction][2]; //mRNA YFP1
                                x[3]+=S[reaction][3]; //YFP1
                                x[4]+=S[reaction][4]; //mRNA CFP2
                                x[5]+=S[reaction][5]; //CFP2
                                x[6]+=S[reaction][6]; //mRNA YFP2
                                x[7]+=S[reaction][7]; //YFP2
                            }
                        }
                        CFP1_NSS[step]=CFP1_NSSold[step]+((x[1]-CFP1_NSSold[step])/double(k));
                        YFP1_NSS[step]=YFP1_NSSold[step]+((x[3]-YFP1_NSSold[step])/double(k));
                        CFP2_NSS[step]=CFP2_NSSold[step]+((x[5]-CFP2_NSSold[step])/double(k));
                        YFP2_NSS[step]=YFP2_NSSold[step]+((x[7]-YFP2_NSSold[step])/double(k));
                        CFP1YFP1_NSS[step]=CFP1YFP1_NSSold[step]+((x[1]*x[3]-CFP1YFP1_NSSold[step])/double(k));
                        CFP2YFP2_NSS[step]=CFP2YFP2_NSSold[step]+((x[5]*x[7]-CFP2YFP2_NSSold[step])/double(k));
                        CFP1YFP2_NSS[step]=CFP1YFP2_NSSold[step]+((x[1]*x[7]-CFP1YFP2_NSSold[step])/double(k));
                        CFP2YFP1_NSS[step]=CFP2YFP1_NSSold[step]+((x[3]*x[5]-CFP2YFP1_NSSold[step])/double(k));
                        CFP1CFP2_NSS[step]=CFP1CFP2_NSSold[step]+((x[1]*x[5]-CFP1CFP2_NSSold[step])/double(k));
                        YFP1YFP2_NSS[step]=YFP1YFP2_NSSold[step]+((x[3]*x[7]-YFP1YFP2_NSSold[step])/double(k));
                        CFP1_NSSold[step]=CFP1_NSS[step];
                        YFP1_NSSold[step]=YFP1_NSS[step];
                        CFP2_NSSold[step]=CFP2_NSS[step];
                        YFP2_NSSold[step]=YFP2_NSS[step];
                        CFP1YFP1_NSSold[step]=CFP1YFP1_NSS[step];
                        CFP2YFP2_NSSold[step]=CFP2YFP2_NSS[step];
                        CFP1YFP2_NSSold[step]=CFP1YFP2_NSS[step];
                        CFP2YFP1_NSSold[step]=CFP2YFP1_NSS[step];
                        CFP1CFP2_NSSold[step]=CFP1CFP2_NSS[step];
                        YFP1YFP2_NSSold[step]=YFP1YFP2_NSS[step];
                    }
                } // END INDIVIDUAL TRAJECTORIES
                for (int r=0; r<rows; r++) {
                    CFP1_SS[r]=CFP1_SSold[r]+((CFP1_NSS[r]-CFP1_SSold[r])/double(j));
                    YFP1_SS[r]=YFP1_SSold[r]+((YFP1_NSS[r]-YFP1_SSold[r])/double(j));
                    CFP2_SS[r]=CFP2_SSold[r]+((CFP2_NSS[r]-CFP2_SSold[r])/double(j));
                    YFP2_SS[r]=YFP2_SSold[r]+((YFP2_NSS[r]-YFP2_SSold[r])/double(j));
                    CFP1YFP1_SS[r]=CFP1YFP1_SSold[r]+((CFP1YFP1_NSS[r]-CFP1YFP1_SSold[r])/double(j));
                    CFP2YFP2_SS[r]=CFP2YFP2_SSold[r]+((CFP2YFP2_NSS[r]-CFP2YFP2_SSold[r])/double(j));
                    CFP1YFP2_SS[r]=CFP1YFP2_SSold[r]+((CFP1YFP2_NSS[r]-CFP1YFP2_SSold[r])/double(j));
                    CFP2YFP1_SS[r]=CFP2YFP1_SSold[r]+((CFP2YFP1_NSS[r]-CFP2YFP1_SSold[r])/double(j));
                    CFP1CFP2_SS[r]=CFP1CFP2_SSold[r]+((CFP1CFP2_NSS[r]-CFP1CFP2_SSold[r])/double(j));
                    YFP1YFP2_SS[r]=YFP1YFP2_SSold[r]+((YFP1YFP2_NSS[r]-YFP1YFP2_SSold[r])/double(j));
                    CFP1_SSold[r]=CFP1_SS[r];
                    YFP1_SSold[r]=YFP1_SS[r];
                    CFP2_SSold[r]=CFP2_SS[r];
                    YFP2_SSold[r]=YFP2_SS[r];
                    CFP1YFP1_SSold[r]=CFP1YFP1_SS[r];
                    CFP2YFP2_SSold[r]=CFP2YFP2_SS[r];
                    CFP1YFP2_SSold[r]=CFP1YFP2_SS[r];
                    CFP2YFP1_SSold[r]=CFP2YFP1_SS[r];
                    CFP1CFP2_SSold[r]=CFP1CFP2_SS[r];
                    YFP1YFP2_SSold[r]=YFP1YFP2_SS[r];
                }
                
            } //END INTIAL CONDITIONS
            for (int r=0; r<rows; r++) {
                CFP1_S[r]=CFP1_Sold[r]+((CFP1_SS[r]-CFP1_Sold[r])/double(i));
                YFP1_S[r]=YFP1_Sold[r]+((YFP1_SS[r]-YFP1_Sold[r])/double(i));
                CFP2_S[r]=CFP2_Sold[r]+((CFP2_SS[r]-CFP2_Sold[r])/double(i));
                YFP2_S[r]=YFP2_Sold[r]+((YFP2_SS[r]-YFP2_Sold[r])/double(i));
                CFP1YFP1_S[r]=CFP1YFP1_Sold[r]+((CFP1YFP1_SS[r]-CFP1YFP1_Sold[r])/double(i));
                CFP2YFP2_S[r]=CFP2YFP2_Sold[r]+((CFP2YFP2_SS[r]-CFP2YFP2_Sold[r])/double(i));
                CFP1YFP2_S[r]=CFP1YFP2_Sold[r]+((CFP1YFP2_SS[r]-CFP1YFP2_Sold[r])/double(i));
                CFP2YFP1_S[r]=CFP2YFP1_Sold[r]+((CFP2YFP1_SS[r]-CFP2YFP1_Sold[r])/double(i));
                CFP1CFP2_S[r]=CFP1CFP2_Sold[r]+((CFP1CFP2_SS[r]-CFP1CFP2_Sold[r])/double(i));
                YFP1YFP2_S[r]=YFP1YFP2_Sold[r]+((YFP1YFP2_SS[r]-YFP1YFP2_Sold[r])/double(i));
                CFP1_Sold[r]=CFP1_S[r];
                YFP1_Sold[r]=YFP1_S[r];
                CFP2_Sold[r]=CFP2_S[r];
                YFP2_Sold[r]=YFP2_S[r];
                CFP1YFP1_Sold[r]=CFP1YFP1_S[r];
                CFP2YFP2_Sold[r]=CFP2YFP2_S[r];
                CFP1YFP2_Sold[r]=CFP1YFP2_S[r];
                CFP2YFP1_Sold[r]=CFP2YFP1_S[r];
                CFP1CFP2_Sold[r]=CFP1CFP2_S[r];
                YFP1YFP2_Sold[r]=YFP1YFP2_S[r];
            }
        } //END SIGMAS (DAUGTHER)
        for (int r=0; r<rows; r++) {
            CFP1[r]=CFP1old[r]+((CFP1_S[r]-CFP1old[r])/double(h));
            YFP1[r]=YFP1old[r]+((YFP1_S[r]-YFP1old[r])/double(h));
            CFP2[r]=CFP2old[r]+((CFP2_S[r]-CFP2old[r])/double(h));
            YFP2[r]=YFP2old[r]+((YFP2_S[r]-YFP2old[r])/double(h));
            CFP1YFP1[r]=CFP1YFP1old[r]+((CFP1YFP1_S[r]-CFP1YFP1old[r])/double(h));
            CFP2YFP2[r]=CFP2YFP2old[r]+((CFP2YFP2_S[r]-CFP2YFP2old[r])/double(h));
            CFP1YFP2[r]=CFP1YFP2old[r]+((CFP1YFP2_S[r]-CFP1YFP2old[r])/double(h));
            CFP2YFP1[r]=CFP2YFP1old[r]+((CFP2YFP1_S[r]-CFP2YFP1old[r])/double(h));
            CFP1CFP2[r]=CFP1CFP2old[r]+((CFP1CFP2_S[r]-CFP1CFP2old[r])/double(h));
            YFP1YFP2[r]=YFP1YFP2old[r]+((YFP1YFP2_S[r]-YFP1YFP2old[r])/double(h));
            CFP1old[r]=CFP1[r];
            YFP1old[r]=YFP1[r];
            CFP2old[r]=CFP2[r];
            YFP2old[r]=YFP2[r];
            CFP1YFP1old[r]=CFP1YFP1[r];
            CFP2YFP2old[r]=CFP2YFP2[r];
            CFP1YFP2old[r]=CFP1YFP2[r];
            CFP2YFP1old[r]=CFP2YFP1[r];
            CFP1CFP2old[r]=CFP1CFP2[r];
            YFP1YFP2old[r]=YFP1YFP2[r];
        }

    } //END SIGMAS (MOTHER)
    for (int r=0; r<rows; r++) { //Print mean of numSigma parameters
        posOut1<<CFP1[r]<<"\t"<<YFP1[r]<<"\t"<<CFP2[r]<<"\t"<<YFP2[r]<<"\t"<<CFP1YFP1[r]<<"\t"<<CFP2YFP2[r]<<"\t"<<CFP1YFP2[r]<<"\t"<<CFP2YFP1[r]<<"\t"<<CFP1CFP2[r]<<"\t"<<YFP1YFP2[r]<<endl;
    }
    delete CFP1;
    delete CFP2;
    delete YFP1;
    delete YFP2;
    delete CFP1old;
    delete CFP2old;
    delete YFP1old;
    delete YFP2old;
    delete CFP1YFP1;
    delete CFP1YFP1old;
    delete CFP1YFP2;
    delete CFP1YFP2old;
    delete CFP2YFP1;
    delete CFP2YFP1old;
    delete CFP2YFP2;
    delete CFP2YFP2old;
    delete YFP1YFP2;
    delete YFP1YFP2old;
    delete CFP1CFP2;
    delete CFP1CFP2old;
    posOut1.close();
    posOut2.close();
    exit(0);
}





