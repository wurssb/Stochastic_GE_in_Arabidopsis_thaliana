Stochastic_GE_in_Arabidopsis_thaliana
================================

Author:		Emma Keizer <emma [dot] keizer [at] wur [dot] nl>
Date:		3 November 2017
Requirements:	C++ >= C++11

To compile script: 
g++ -std=c++11 SCRIPT_NAME.cpp -o YOUR_OUTPUT_NAME

To run script:
 ./YOUR_OUTPUT_NAME

All contents of this folder refers to “Stochastic gene expression in Arabidopsis thaliana” by Araujo et al. (Nature Communications). 

The enclosed scripts simulate data used to calculate the non-stationary autocorrelation of the linear two-stage model of the KikGR system (Autocorrelation_2stage_model.cpp), and the cell-cell correlation of two cells that originate from the same mother cell using a dual reporter approach (Cell_cell_correlation_2stage_model.cpp). The stochastic translation rate is a source of extrinsic noise, for both the Autocorrelation as well as for the Cell-cell correlation simulations.

Stochastic trajectories are simulated using the Extrande method by Voliotis et al. (PLOS Computational Biology, 12(6): e1004923, 2016). For more information on the simulation procedure and calculation of the autocorrelation and cell-cell correlation, refer to Supplementary Note 1 and 4 of “Stochastic gene expression in Arabidopsis thaliana” by Araujo et al., respectively.

Autocorrelation_2stage_model.cpp:
The script generates 4 output files:
- GFP_t1.txt: Expected value of the reporter GFP at time t1, <GFPt1> 
- GFP_t1_t1.txt: Expected value of the reporter GFP at time t1 (GFPt1) squared, <GFPt1*GFPt1>
- GFP_t1_t2.txt: Expected value of the product of the reporter GFP at time t1 (GFPt1) and time t2 (GFPt2), <GFPt1*GFPt2>
- Simulation_settings.txt: Contains settings (e.g. variance of translation rate) that were used in the simulation

Cell_cell_correlation_2stage_model.cpp:
The script generates 2 output files:
- EX.txt: Contains 10 columns with the following expected values at each time point: <CFP1> <CFP2> <YFP1> <YFP2> <CFP1*YFP1> <CFP2*YFP2> <CFP1*YFP2> <CFP2*YFP1> <CFP1*CFP2> <YFP1*YFP2>, for the dual reporter pairs CFP1 and YFP1, and CFP2 YFP2.
- Simulation_settings.txt: Contains settings (e.g. variance of translation rate) that were used in the simulation
